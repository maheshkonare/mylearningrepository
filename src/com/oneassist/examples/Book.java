package com.oneassist.examples;

import java.util.Date;

class Book {
	private int id;
	private String name;
	private String author;
	private String publisher;
	private String language;
	private double price;
	private Date publishedDate;

	void setId(int givenId)
	{
		this.id=givenId;
	}
	int getId()
	{
		return this.id;
	}
	void setName(String gname)
	{
		this.name=gname;
	}
	String getName()
	{
		return this.name;
	}
	String getAuthor() {
		return author;
	}
	void setAuthor(String author) {
		this.author = author;
	}
	String getPublisher() {
		return publisher;
	}
	void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	String getLanguage() {
		return language;
	}
	void setLanguage(String language) {
		this.language = language;
	}
	double getPrice() {
		return price;
	}
	void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() 
	{
		return "my book is [name=" +this.name+", Author="+ this.author + "Book price=" + this.price+"]";
	}
	
	Date getPublishedDate() {
		return publishedDate;
	}
	void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}




}


