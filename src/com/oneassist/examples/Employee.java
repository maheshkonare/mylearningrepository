package com.oneassist.examples;

class Employee {
	private int id;
	private String name;
	private String dept;
	
	Employee()
	{
		
	}

public Employee(int id,String name,String dept)
{
	// TODO Auto-generated constructor stub
	this.id=id;
	this.name=name;
	this.dept=dept;
	
}
	int getId()
	{
		return this.id;
	}
	void setId(int id)
	{
		this.id=id;
	}
	String getName() {
		return name;
	}
	void setName(String name) {
		this.name = name;
	}
	String getDept() {
		return dept;
	}
	void setDept(String dept) {
		this.dept = dept;
	}
	
public String toString()
{
	return " "+this.id+" "+ this.name+ " " + this.dept;
}


}
