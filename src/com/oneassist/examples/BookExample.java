package com.oneassist.examples;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.oneassist.examples.Book;
public class BookExample {
	
	double getTotalPrice(Set<Book> books)
	{
		double sum=0;
		for(Book b: books )
		{
			sum=sum+b.getPrice();
			
		}
		return sum;
	}
	
	double getPrice(Set<Book> books,int id)
	{
		for(Book b: books)
		{
			if(b.getId()==id)
				return b.getPrice();
		}
		return 0;
		
	}
	
	boolean isBookAvailable(Set<Book> books,String bookName)
	{
		for(Book b: books)
		{
			 if(b.getName()== bookName)
				return true;
		}
		return false;
	}
	 
	int countBooksByAuthor(Set<Book> books,String author)
	{
		int count=0;
		for(Book b: books)
		{
			if(b.getAuthor()== author)
				count=count+1;
			
		}
		return count;
	}
	
	List<String> getBookNamesByAuthor(Set<Book> books,String author)
	{
		List<String> result=new ArrayList<String>();
		for(Book b: books)
		{
			if(b.getAuthor()== author)
			{
				result.add(b.getName());
			}
			
		}
		return result;
	}
	
	Map<String,Integer> bookCountByAuthor(Set<Book> books)
	{
		Map<String,Integer> res=new TreeMap<String,Integer>();
		for(Book b: books)
		{
			if(res.containsKey(b.getAuthor()))
			{
				int count=res.get(b.getAuthor());
				count++;
				res.put(b.getAuthor(), count);
			}
			else
			{
				res.put(b.getAuthor(), 1);
			}
		}
		return res;
	}
 	
	public static void main(String[] args)
	{
		
		Book b1=new Book();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

		String dateInString = "1-Jan-2019";
		Date b1date=null;
		try {
			  b1date= formatter.parse(dateInString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		b1.setId(1);
		b1.setName("C");
		b1.setAuthor("BKS");
		b1.setPublisher("Sapna");
		b1.setLanguage("English");
		b1.setPrice(120.30);
		//b1.setPublishedDate(new Date("01/01/2019"));
		b1.setPublishedDate(b1date);
		
		System.out.println(b1);
		
//B2
		
		Book b2=new Book();
		//SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

		//String dateInString ;
		Date b2date=null;
		try {
			  b2date= formatter.parse("3-Nov-2009");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		b2.setId(2);
		b2.setName("C++");
		b2.setAuthor("SPB");
		b2.setPublisher("Aadarsh");
		b2.setLanguage("English");
		b2.setPrice(150.85);
		//b1.setPublishedDate(new Date("01/01/2019"));
		b2.setPublishedDate(b2date);
		
		System.out.println(b2);

		//B3
		
		Book b3=new Book();
		//SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

		//String dateInString = ;
		Date b3date=null;
		try {
			  b3date= formatter.parse("28-Feb-2012");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		b3.setId(3);
		b3.setName("Java");
		b3.setAuthor("SPB");
		b3.setPublisher("Sapna");
		b3.setLanguage("Hindi");
		b3.setPrice(450);
		//b1.setPublishedDate(new Date("01/01/2019"));
		b3.setPublishedDate(b3date);
		
		System.out.println(b3);
		
		//Set
		
		Set<Book> booksCollection= new HashSet<Book>();
		booksCollection.add(b1);
		booksCollection.add(b2);
		booksCollection.add(b3);
		
		System.out.println(booksCollection);
		
		//Number of books available is
		
		System.out.println("Number of books available is "+ booksCollection.size());
		
		//Total price of book
		
		BookExample be=new BookExample();
		
		System.out.println("Total price of all book is "+ be.getTotalPrice(booksCollection));
		
		System.out.println("Price of book2 is "+ be.getPrice(booksCollection,2));
		
		System.out.println("Price of book4 is "+ be.getPrice(booksCollection,4));
		 
		System.out.println("C++ availabe ? " +be.isBookAvailable(booksCollection, "C++"));
		
		System.out.println("ASP.net availabe ? " +be.isBookAvailable(booksCollection, "ASP.net"));
		
		System.out.println("number of books by SPB are= "+ be.countBooksByAuthor(booksCollection, "SPB"));
		
		System.out.println("Books by SPB"+be.getBookNamesByAuthor(booksCollection, "SPB"));
		
		System.out.println("Books count by author"+ be.bookCountByAuthor(booksCollection));

	}
	

}
