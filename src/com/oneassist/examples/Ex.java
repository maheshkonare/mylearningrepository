package com.oneassist.examples;
import java.util.HashMap;
import java.util.Map;

import com.oneassist.examples.*;
public class Ex {
	
	String name;
	int id;
	
	Ex()
	{
		
	}
	Ex(String name,int id)
	{
		this.name=name;
		this.id=id;
	}
	
	@Override
	public boolean equals(Object obj) {
		Ex other = (Ex) obj;
		return this.name== other.name;
	}
	@Override
	public String toString() {
		return this.id + this.name;
	}
	

	@Override
	public int hashCode() {
		return this.id%10;
	}
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String name;
		Ex em1=new Ex("Mahesh",1);
		Ex em2=new Ex("Mahesh",11);
		Ex em3=new Ex("Devika",2);
		Ex em4 = new Ex();
		
		Map<Ex, Ex> res=new HashMap<Ex, Ex>();
		
		res.put(em1, em1);
		res.put(em2, em2);
		res.put(em3, em3);
		System.out.println(res);
//		System.out.println(em1.equals(em2));
//		System.out.println(em1==em2);
//		System.out.println(em1);
//		System.out.println(em3);
//		System.out.println(em3.hashCode());
//		System.out.println(em1.hashCode());
//		System.out.println("Current memory address for first object is="+em1.hashCode());
//		System.out.println("Current memory address for Second object is="+em2.hashCode());
//		System.out.println("After overriding Hashcode method memory address of both the objects are given below");
//		
//		System.out.println(em1.hashCode()+"\n"+em2.hashCode());
//		System.out.println(res);
		
	}

}
