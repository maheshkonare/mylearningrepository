package com.oneassist.examples;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.oneassist.examples.Employee;

public class EmployeeExample {
	
	int employeeCountByDept(Set<Employee> emps,String dept)
	{
		int count=0;
		for(Employee emp: emps)
		{
			if(emp.getDept()== dept)
			{
				count++;
			}
		}
		return count;
		
	}
	
	Set<String> empNamesByDept(Set<Employee> emps,String dept)
	{
		Set<String> res=new TreeSet<String>();
		for(Employee emp:emps)
		{
			if(emp.getDept()==dept)
			{
				res.add(emp.getName());
				
			}
		}
		return res;
	}
	
	Map<String, List<String>> deptWiseEmployeeNames(Set<Employee> emps)
	
	{
		Map<String, List<String>> result= new HashMap<String, List<String>>();
		
		for(Employee emp: emps)
		{
			
			if(result.containsKey(emp.getDept()))
			{
				List<String> ls2=result.get(emp.getDept());
				ls2.add(emp.getName());
				result.put(emp.getDept(), ls2);
				
			}
			else
			{
				List<String> ls1=new ArrayList<String>();
				ls1.add(emp.getName());
				result.put(emp.getDept(), ls1);
				
				
			}
		}
		
		return result;
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee emp1=new Employee();
		emp1.setId(1);
		emp1.setName("A");
		emp1.setDept("D1");

		System.out.println(emp1);

		Employee emp2=new Employee();
		emp2.setId(2);
		emp2.setName("B");
		emp2.setDept("D2");

		System.out.println(emp2);

		Employee emp3=new Employee();
		emp3.setId(3);
		emp3.setName("C");
		emp3.setDept("D1");

		System.out.println(emp3);

		Employee emp4=new Employee();
		emp4.setId(4);
		emp4.setName("D");
		emp4.setDept("D1");

		System.out.println(emp4);

		Employee emp5=new Employee();
		emp5.setId(5);
		emp5.setName("E");
		emp5.setDept("D2");

		System.out.println(emp5);
		
		Employee emp6=new Employee(6,"F","D2");

		Set<Employee> empSet=new HashSet<Employee>();
		empSet.add(emp1);
		empSet.add(emp2);
		empSet.add(emp3);
		empSet.add(emp4);
		empSet.add(emp5);
		empSet.add(emp6);

		System.out.println(empSet);

EmployeeExample em=new EmployeeExample();


System.out.println("Number employees present in the given department are=" + em.employeeCountByDept(empSet, "D1"));

System.out.println("Department Employee names"+ em.empNamesByDept(empSet, "D2"));

System.out.println("Departmentwise employees are=" +em.deptWiseEmployeeNames(empSet));



	}


}


