package com.oneassist.assetmanagement;

import java.util.HashMap;

public class AntGame {
	public static void main(String[] strs)
	{
		String sentence= "an ant came to my home to say bye";
		String[] words=sentence.split(" ");//word[0]=an word[1]=ant
		HashMap<Character,Integer> hsh=new HashMap<Character,Integer>();
		for(int i=0;i<words.length;i++)
		{
			if(words[i].startsWith("a"))
			{
				if(hsh.containsKey('a'))
				{
					int count=hsh.get('a');
					count++;
					hsh.put('a', count);
				}
				else {
					hsh.put('a',1);
				}
			}
			if(words[i].startsWith("b"))
			{
				if(hsh.containsKey('b'))
				{
					int count=hsh.get('b');
					count++;
					hsh.put('b', count);
				}
				else {
					hsh.put('b',1);
				}
			}
			if(words[i].startsWith("c"))
			{
				if(hsh.containsKey('c'))
				{
					int count=hsh.get('c');
					count++;
					hsh.put('c', count);
				}
				else {
					hsh.put('c',1);
				}
			}
		}
		System.out.println(hsh);
	}
}
