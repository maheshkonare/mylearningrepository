package com.oneassist.assetmanagement;

public class MaxNumberInArray {
	public static void main(String[] args)
	{
		int[] numbers= {-1,-2,-3,-4};
		int max=0;
		for(int i=0;i<numbers.length;i++)
			if(max > numbers[i])
			{
				max=numbers[i];
			}

		System.out.println("maximum number in an given array is"+max);
	}
}
