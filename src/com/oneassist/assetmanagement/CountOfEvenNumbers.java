package com.oneassist.assetmanagement;

import java.util.HashMap;

public class CountOfEvenNumbers {

	public static void main(String[] strn)
	{
		int[] numbers= {1,1,2,3,4,4,6,9};
		HashMap<Integer,Integer> hsh=new HashMap<Integer,Integer>();
		int div;

		for(int i=0;i<numbers.length;i++)
		{
			div=numbers[i]%2;
			if(div==0)
			{
				if(hsh.containsKey(numbers[i]))
				{
					int count=hsh.get(numbers[i]);
					count++;
					hsh.put(numbers[i], count);
				}
				else
				{
					hsh.put(numbers[i], 1);
				}
			}
		}
		System.out.println(hsh );

	}
}
