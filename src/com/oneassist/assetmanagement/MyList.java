package com.oneassist.assetmanagement;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

public class MyList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> list1=new ArrayList<Integer>(5);
		list1.add(1);
		list1.add(12);
		list1.add(2, 20);
		//list1.remove(2);
		int l=list1.get(0);
		System.out.println(list1);
		System.out.println("0th element is "+l);
		System.out.println("Array Elements are=");
		for(Integer lis: list1 )
		{
			System.out.println(lis);
		}
		
//Linked list
		System.out.println("Linked list operation");
		List<Integer> list2 = new LinkedList<Integer>();
		list2.add(2);
		list2.add(4);
		list2.add(8);
		list2.add(10);
		for(int num:list2)
		{
			System.out.println(num);
		}
		System.out.println("3rd element in the array is"+ list2.get(2));
		int update=list2.set(3, 12);
		//System.out.println("4th element is updated"+ update);
		//list2.remove(2);
		for(int num:list2)
		{
			System.out.println(num);
		}
		System.out.println("------------------------");
		list2.remove(1);
		for(int num:list2)
		{
			System.out.println(num);
		}
		
		//Vector
		
		System.out.println("Vector");
		
		List<Integer> list3=new Vector<Integer>();
		list3.add(2);
		list3.add(4);
		list3.add(8);
		list3.add(10);
		for(int num3: list3)
		{
			System.out.println(num3);
		}
		//Stack
		
		System.out.println("Stack operation");
		List<Integer> list4=new Stack<Integer>();
		list4.add(1);
		list4.add(3);
		list4.add(5);
		list4.add(7);
		list4.add(9);
		for(int num: list4)
		{
			System.out.println(num);
		}
		System.out.println("Size of the stack is "+ list4.size());
		System.out.println("3rd element is "+ list4.get(2));
		System.out.println("top element is "+ list4.get(list4.size()-1));
		System.out.println("popped element is "+ list4.remove(list4.size()-1));
		
		list4.clear();
		for(int num: list4)
		{
			System.out.println(num);
		}
	}
	
	

	
	

}
