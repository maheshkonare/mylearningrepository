package com.oneassist.assetmanagement;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Mahesh
 *
 */
//set1 = [1, 3, 2, 4, 8, 9, 0] 
//set2 = [1, 3, 7, 5, 4, 0, 7, 5]

public class HashSetExample {
	
	public static void main(String[] args)
	{
		Integer[] set1= {1,3,2,4,8,9,0};
		Integer[] set2= {1,3,7,5,4,0,7,5};
		
	Set<Integer> mySet1=new HashSet<Integer>();
mySet1.addAll(Arrays.asList(set1));
System.out.println("First set s1="+mySet1);
Set<Integer> mySet2=new HashSet<Integer>();
mySet2.addAll(Arrays.asList(set2));
System.out.println("Second set s2="+mySet2);
		
//union
Set<Integer> union=new HashSet<Integer>();
union.addAll(mySet1);
union.addAll(mySet2);
System.out.println("union="+union);

//Intersection
Set<Integer> intersection=new HashSet<Integer>();

intersection.addAll(mySet1);
intersection.retainAll(mySet2);
System.out.println("Intersection="+intersection);

//Difference
Set<Integer> difference=new HashSet<Integer>();
difference.addAll(mySet1);
difference.removeAll(mySet2);
System.out.println("Difference="+difference);

//Iterate
for(Integer i: difference)
{
	System.out.println(i);
}

//Linked HashSet
Set<String> countries=new LinkedHashSet<String>();
countries.add("india");
countries.add("Australia");
countries.add("Newzealand");
countries.add("Srilnaka");
//for(String country: countries)
//{
//	System.out.println(country);
//}
//Tree Set
Set<String> treeCountries=new TreeSet<String>();
treeCountries.add("india");
treeCountries.add("australia");
treeCountries.add("newzealand");
treeCountries.add("srilanka");
for(String treecountry: treeCountries)
{
	System.out.println(treecountry);	
}



	}
}