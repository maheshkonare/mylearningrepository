package com.oneassist.assetmanagement;

import java.util.HashMap;

public class CountOfVowels {

	public static void main(String[] args)
	{
		String sen="hi this is devika";
		HashMap<Character,Integer> vowelsCount= new HashMap<Character,Integer>();



		for(int i=0;i<sen.length();i++)
		{
			char letter=sen.charAt(i);

			if(letter== 'a' || letter == 'e' || letter== 'i' || letter== 'o' || letter== 'u')
			{
				if(vowelsCount.containsKey(letter))
				{
					int currentCount=vowelsCount.get(letter);
					currentCount++;
					vowelsCount.put(letter, currentCount);
				}
				else
				{
					vowelsCount.put(letter, 1);
				}

			}

		}
System.out.println(vowelsCount);

	}

}

