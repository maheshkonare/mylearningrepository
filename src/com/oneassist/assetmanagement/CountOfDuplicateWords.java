package com.oneassist.assetmanagement;

import java.util.HashMap;

public class CountOfDuplicateWords {




	public static void main(String[] str)
	{
		String sen= "apple mango apple app lem";
		HashMap<String,Integer> count=new HashMap<String,Integer>();
		String regex = " ";
		String[] words=sen.split(regex);
		for(int i=0; i<words.length;i++)
		{
			String word=words[i];
			int size=word.length();
			if(!(size>4))
			{continue;}
			if(count.containsKey(word))
			{
				int currentcount= count.get(word);

				currentcount++;
				count.put(word, currentcount);
			}else {
				count.put(word, 1);
			}

		}
		System.out.println(count);
	}
}
