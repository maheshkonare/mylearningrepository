package com.oneassist.assetmanagement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class CollectionExample {
	//TODO need to implement for treemap , Linkedhashmap; linkedlist
	

/**


Parent P = new Child(); // small thing. Can be assigned to biggerthing
Child c= new Parent()// NOT POSSIBLE, because Animal is not Elephant

When you say �object.�it sholws all methods. From that particularclass type And also methods from parent class if thereis. Inheritance

Downcasting: converting from Parent  reference  type to child

Animal a = new Animal();
Elephant e =  (Elephant)a;

Upcasting: converting from Child reference type to. Parent
Elephant e = new Elephant();
Animal a = (Animal) e;

Inheritance rules based on following access modifiers.
Private
Default /package level
Protected
Public


DataStructures:

Publi static void main(String[] varName){
}

String[] varName = new String[15];


Collections:



Set (Interface which a parent interface) -> only unique elemetns
Set -> (HashSet,TreeSet,LinkedHashSet) -> Contains only unique elements

Set s = new HashSet(); - contains only unique elements, , no ordering elements
Set s = new TreeSet(); - contains only unique elements and also maintains in asc order,  no ordering elements
Set s = new LinkedHashSet();- contains only unique elements, it maintains the. Order in which you add elements

Class HashSet implements Set {

}

HashSet hs = new HashSet();
TreeSet ts = new TreeSet();


HashSet<String> hs = new HashSet<String>();
hs.add(�mahesh);
hs.add(�srikanth);
hs.add(�mahesh);
hs.add(�srikanth);

mahesh, srikanth. -> only unique ements will be stored



TreeSet<String> ts = new TreeSet<String>();
String value = �bc�;
ts.add(value);
ts.add(�bc�);
ts.add(�ac�);
ts.add(�ac�);


LinkedHashSet<String> a = new LinkedHashSet<String>();
a.add(�b�);
a.add(�a�);
a.add(�c�);
a.add(�c�);
a.add(�b�);

result: b,a,c



k,v

�name�=�mahesh�

Map -> (HashMap, TreeMap, LinkedHashMap) � does not allow duplicates

HashMap -> does not allow duplicates
TreeMap -> does not allow duplicates, sorted
LinkedHashMap -> does not allow duplicates but maintains order of insertion

HashMap<String, String> map = new HashMap<String, String>();
map.put(�name�, �mahesh�);
m.get(�name�);//result = mahesh

TreeMap<String, String> map = new TreeMap<String, String>();
map.put(�name�, �mahesh�);
map.put(�age�, �26�);

Result =age entry comes first because Age word is lesser


LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
map.put(�name�, �mahesh�);
map.put(�age�, �26�);


name,  age entry will come

 
map.put(�name�, �s�);
s.add(�mahesh�);


List -> (ArrayList, LinkedList)

ArrayList<String> l = new ArrayList<String>();
l.add(�a�);
l.add(�b�);
l.add(�c�);

LikedList<String> l = new LikedList <String>();
l.add(�a�);
l.add(�b�);
l.add(�c�);


String[] a = new String[30];


HashSet<Employee> s = new HashSet<Employee>();
s.add(new Employeee());


Primitive data types - Wrapper
byte -> Byte
short -> Short
int -> Integer
long  -> Long
float- > FLoat
double -> Double
boolean - Boolean
char- > Character

Custom type
String




Collection Framework -> Set, Map, List
*/

	
/*
 * Input = sentense
 * 
 * SPlit. By words And print cound of word
 */

	
	public ArrayList<String> getArrayElements(String[] names)
	{

		ArrayList<Integer> x  =new  ArrayList<Integer>();
		x.add(1);

		 ArrayList<String> arraylst=new  ArrayList<String>();
		 for(int i=0;i<names.length;i++)
			{
				arraylst.add(names[i]);
			}
		 return arraylst;
		 
	}
	
	public HashMap<String,String> getUniqueElementsWithKeyValue(String[] names)
	{
		HashMap<String,String> hm=new HashMap<String,String>();
		for(int i=0;i<names.length;i++)
		{
			hm.put(names[i], names[i]);
		}
		return hm;
	}
	public LinkedHashSet<String> getUniqueInsertionOrder(String[] names)
	{
		LinkedHashSet<String> linkedset=new LinkedHashSet<String>();
		for(int i=0;i<names.length;i++)
		{
			linkedset.add(names[i]);
		}
		
		return linkedset;
		
	}
	
public TreeSet<String> getUniqueSortedElements(String[] names)
{
 	TreeSet<String> tree=new TreeSet<String>();
 	for(int i=0;i<names.length;i++)
		{
			tree.add(names[i]);
		}
 	return tree;
 	
}
	
	public HashSet<String> getUniqueElements(String[] names)
	{
 		HashSet<String> hash=new HashSet<String>();
 		for(int i=0;i<names.length;i++)
 		{
 			hash.add(names[i]);
 		}
 		return hash;
	}
public static void main(String[] args)
{
	CollectionExample ce=new CollectionExample();
	String[] names= {"mahesh","asrikanth","mahesh"};
	HashSet<String> result=ce.getUniqueElements(names);
	TreeSet<String> treeResult=ce.getUniqueSortedElements(names);
	LinkedHashSet<String> linkedset=ce.getUniqueInsertionOrder(names);
	HashMap<String,String> hashmap=ce.getUniqueElementsWithKeyValue(names);
	ArrayList<String> arrlst=ce.getArrayElements(names);
	//for(int i=0;i<result.size();i++)
	//{
		System.out.println(result);
	//}
		System.out.println(treeResult);
		System.out.println(linkedset);
		System.out.println(hashmap);
		System.out.println(arrlst);
}
}
