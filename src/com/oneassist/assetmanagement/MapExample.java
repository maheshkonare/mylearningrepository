package com.oneassist.assetmanagement;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Map<Integer,String> mapExample= new HashMap<Integer,String>();
		mapExample.put(1, "abc");
		mapExample.put(2, "def");
		mapExample.put(3, "xyz");
		System.out.println(mapExample);
		
		//2
		System.out.println(mapExample.get(3));
		//3
		
		System.out.println(mapExample.containsKey(4));
		System.out.println(mapExample.containsKey(2));
		System.out.println(mapExample.containsValue("def"));
		mapExample.replace(3, "ijk");
		System.out.println(mapExample);
		mapExample.remove(3);
		System.out.println(mapExample);
		
		
		
		//Linked Hashset
		
		Map<String,String> mapExample2=new TreeMap<String,String>();
		mapExample2.put("Sachin", "Tendulkar");
		mapExample2.put("VVS", "Laxman");
		mapExample2.put("Yuvraj", "Singh");
		mapExample2.put("Dale", "Steyn");
		int i=1;
		System.out.println("My Favourie cricketers are");
		//System.out.println(mapExample2);
		for(Map.Entry<String, String> entry: mapExample2.entrySet())
		{
			
			System.out.println(i+". "+entry.getKey()+ " "+ entry.getValue());
			i++;
		}
		
	}

}
